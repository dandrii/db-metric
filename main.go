package main

import (
	"bufio"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"syscall"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"golang.org/x/crypto/pbkdf2"
	"golang.org/x/crypto/ssh/terminal"
	"gopkg.in/yaml.v3"
)

var resetRing = flag.Bool("reset-connection-string", false, "If this flag is set you will be asked to enter connection string once again")
var metricName = flag.String("metric", "", "Metric name to run, if not provided all metrics from the config will be executed")

type Config struct {
	Name             string
	Output           string
	ConnectionString string `yaml:"connectionString"`
	Metrics          []struct {
		Name  string
		Query string
	}
}

func fatal(err error) {
	log.Println(err)

	fmt.Println("Press Enter to exit..")
	bufio.NewReader(os.Stdin).ReadBytes('\n')

	os.Exit(1)
}

func encrypt(key []byte, text string) (out []byte, err error) {
	c, err := aes.NewCipher(key)
	if err != nil {
		return
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return
	}

	out = gcm.Seal(nonce, nonce, []byte(text), nil)

	return
}

func decrypt(key []byte, in []byte) (text string, err error) {
	c, err := aes.NewCipher(key)
	if err != nil {
		return
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return
	}

	nonceSize := gcm.NonceSize()
	nonce, citext := in[:nonceSize], in[nonceSize:]

	btext, err := gcm.Open(nil, nonce, citext, nil)
	if err != nil {
		return
	}

	text = string(btext)

	return
}

func prompt(msg string) (text string) {
	fmt.Print(msg)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		text = scanner.Text()
		if text != "" {
			return
		}
	}

	return
}

func getConfig() (c Config, err error) {
	data, err := os.ReadFile("config.yaml")
	if err != nil {
		return
	}

	err = yaml.Unmarshal([]byte(data), &c)
	return
}

func getConnectionString() (connStr string, err error) {
	data, err := os.ReadFile(".connstr")

	var pass []byte

	if errors.Is(err, os.ErrNotExist) || *resetRing {
		err = nil

		connStr = prompt("Provide DB connection string\n> ")

		fmt.Print("Create a short password for encryption (you will not see characters as you type)\n> ")
		pass, err = terminal.ReadPassword(int(syscall.Stdin))
		if err != nil {
			return
		}

		salt := make([]byte, 16)
		_, err = rand.Read(salt)
		if err != nil {
			return
		}

		var citext []byte

		key := pbkdf2.Key([]byte(pass), salt, 4096, 32, sha256.New)
		citext, err = encrypt(key, connStr)
		if err != nil {
			return
		}

		var d []byte
		d = append(d, salt...)
		d = append(d, citext...)

		os.WriteFile(".connstr", d, 0777)
		return
	} else if err != nil {
		return
	}

	fmt.Print("Provide encryption password (you will not see characters as you type)\n> ")
	pass, err = terminal.ReadPassword(int(syscall.Stdin))
	if err != nil {
		return
	}

	salt := data[:16]
	citext := data[16:]

	key := pbkdf2.Key([]byte(pass), salt, 4096, 32, sha256.New)
	connStr, err = decrypt(key, citext)
	return
}

func main() {
	flag.Parse()

	connStr, err := getConnectionString()
	if err != nil {
		fatal(err)
	}

	c, err := getConfig()
	if err != nil {
		fatal(err)
	}

	fmt.Println("Connecting to DB..")
	db, err := sqlx.Connect("postgres", string(connStr))
	if err != nil {
		fatal(err)
	}

	for _, m := range c.Metrics {
		if *metricName != "" && *metricName != m.Name {
			return
		}

		fmt.Printf("Running %s..\n", m.Name)

		rows, err := db.Queryx(m.Query)
		if err != nil {
			fatal(err)
		}

		for rows.Next() {
			colNames, err := rows.Columns()
			if err != nil {
				fatal(err)
			}

			cols := make([]interface{}, len(colNames), len(colNames))
			colValues := make([]interface{}, len(colNames), len(colNames))
			for i := range colNames {
				colValues[i] = &cols[i]
			}

			if err = rows.Scan(colValues...); err != nil {
				fatal(err)
			}

			for i, n := range colNames {
				fmt.Printf("\t%s: %+v\n", n, *colValues[i].(*interface{}))
			}
		}
	}

	fmt.Println("\nPress Enter to exit..")
	bufio.NewReader(os.Stdin).ReadBytes('\n')
}
