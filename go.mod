module gitlab.com/dandrii/db-metric

go 1.16

require (
	github.com/jmoiron/sqlx v1.3.3
	github.com/lib/pq v1.10.0
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
