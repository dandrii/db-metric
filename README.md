# DB Metric

## Usage

```sh
./db-metric [--metric name] [--reset-connection-string]
```

## Parameters
- `--metric` - name of metric to run, if omitted all metrics will be executed
- `--reset-connection-string` - will ignore saved in keyring connection string and ask for a new one

## Config file
Config file with the name `config.yaml` should be placed in the current working directory.

File should have following structure:
```yaml
name: DB Metric
output: stdout
metrics:
  - name: name of the metric
    query: SQL query to be executed
```

